// We need to keep this file within the root folder, as service worker scope gives
// access to only folders beneath the scope of the service worker file.

// For Firebase JavaScript SDK v7.20.0 and later, `measurementId` is an optional field
const firebaseConfig = {
    apiKey: "AIzaSyB_WKpcD4_74ISqhLvcZhd81-oRLqCtUbQ",
    authDomain: "pwa-demo-655f5.firebaseapp.com",
    projectId: "pwa-demo-655f5",
    storageBucket: "pwa-demo-655f5.appspot.com",
    messagingSenderId: "752832370139",
    appId: "1:752832370139:web:c3c2c2de798dcb5c9c43fe",
    measurementId: "G-2J1F62ZXB1"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging()
let registration = null;

//Create our service workers
//Register the ServiceWorker for offline access
navigator.serviceWorker.register('offline_service_worker.js', {scope: './'}).then(
    function(reg) {
        console.log('Offline service worker has been registered for scope:'+ reg.scope);
    }
);

//Register the ServiceWorker for notification access
navigator.serviceWorker.register('firebase-messaging-sw.js', {scope: './'}).then(
    function(reg){
        messaging.useServiceWorker(reg)
        registration = reg;
        registerToken();
        console.log("Notification service worker registered for scope: ", reg.scope);
    }
);

function registerToken() {
    messaging.requestPermission().then(() => {
        return messaging.getToken();
    }).then(token => {
        console.log(token)
        document.getElementById("tokenDiv").innerHTML = token
    }).catch(err => {
        console.log(err)
        document.getElementById("errorDiv").innerHTML = err
    })

};

// This is what executes when a message is received in the foreground.
// For background notifications, see firebase-messaging-sw.js#onBackgroundMessage
messaging.onMessage((payload) => {
    if (window.Notification && Notification.permission === "granted") {
        console.log(payload)
        const options = {
            body: payload.notification.body,
            icon: "./icons/icon-192x192.png"
        }

        registration.showNotification(payload.notification.title, options)
    }
});

