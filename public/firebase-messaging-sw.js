importScripts("https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js")
importScripts("https://www.gstatic.com/firebasejs/8.2.1/firebase-firestore.js")
importScripts("https://www.gstatic.com/firebasejs/8.2.1/firebase-messaging.js")

// For Firebase JavaScript SDK v7.20.0 and later, `measurementId` is an optional field
const firebaseConfig = {
    apiKey: "AIzaSyB_WKpcD4_74ISqhLvcZhd81-oRLqCtUbQ",
    authDomain: "pwa-demo-655f5.firebaseapp.com",
    projectId: "pwa-demo-655f5",
    storageBucket: "pwa-demo-655f5.appspot.com",
    messagingSenderId: "752832370139",
    appId: "1:752832370139:web:c3c2c2de798dcb5c9c43fe",
    measurementId: "G-2J1F62ZXB1"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging()

messaging.onBackgroundMessage(function(payload) {
    if(Notification.permission == "granted") {
        console.log('[firebase-messaging-sw.js] Received background message ', payload);
        // Customize notification here
        const notificationTitle = payload.notification.title;
        const notificationOptions = {
            body: payload.notification.body,
            icon: 'icons/favicon-32x32.png'
        };

        //self.registration.showNotification(payload, notificationOptions);
    }
});

